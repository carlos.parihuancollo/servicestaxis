<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Conductor_model extends CI_Model {

	public function retornarConductor()
	{
		$this->db->select('idConductor,primerApellido,segundoApellido,nombres,numeroLicencia,direccion,telefonoFijo,celular');
		$this->db->from('conductor');
		$this->db->order_by('primerApellido','asc');
		return $this->db->get();
	}
	public function eliminarConductor($idConductor)
	{
		$this->db->where('idConductor',$idConductor);
		$this->db->delete('conductor');
	}
	public function agregarConductor($data)
	{
		$this->db->insert('conductor',$data);
	}
	
	public function recuperarConductor($idConductor)
	{
		$this->db->select('idConductor,primerApellido,segundoApellido,nombres,numeroLicencia,direccion,telefonoFijo,celular');
		$this->db->from('conductor');
		$this->db->where('idConductor',$idConductor);
		return $this->db->get();
	}
	public function modificarConductor($idConductor,$data)
	{
		$this->db->where('idConductor',$idConductor);
		$this->db->update('conductor',$data);
	}


}
