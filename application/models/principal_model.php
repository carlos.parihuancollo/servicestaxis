<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Principal_model extends CI_Model {

	public function retornarPaises()
	{
		$this->db->select('*');
		$this->db->from('paises');
		$this->db->order_by('pais','asc');
		return $this->db->get();
	}

	public function eliminarConductor($idpais)
	{
		$this->db->where('idpais',$idpais);
		$this->db->delete('paises');
	}

	
}
