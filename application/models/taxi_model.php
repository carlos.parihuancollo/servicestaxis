<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Taxi_model extends CI_Model {

	public function retornarTaxi()
	{
		$this->db->select('idtaxi,numeroPlaca,numeroMovil,idtipoVehiculo');
		$this->db->from('taxi');
		//$this->db->order_by('','asc');
		return $this->db->get();
	}
	public function eliminarTaxi($idtaxi)
	{
		$this->db->where('idtaxi',$idtaxi);
		$this->db->delete('taxi');
	}
	public function agregarTaxi($data)
	{
		$this->db->insert('taxi',$data);
	}
	
	public function recuperarTaxi($idtaxi)
	{
		$this->db->select('idtaxi,numeroPlaca,numeroMovil,idtipoVehiculo');
		$this->db->from('taxi');
		$this->db->where('idtaxi',$idtaxi);
		return $this->db->get();
	}
	public function modificarTaxi($idtaxi,$data)
	{
		$this->db->where('idtaxi',$idtaxi);
		$this->db->update('taxi',$data);
	}


}
