<div class="container">

<h2>Lista de Conductores</h2>
  
  <?php echo form_open_multipart('principal/agregarConductor'); ?>
          
            <button type="submit" class="btn btn-primary">Agregar</button>
          <?php echo form_close(); ?>


  <table class="table">
    <thead>
      <tr>
        <th>No.</th>
        <th>Primer Apellido</th>
        <th>Segundo Apellido</th>
        <th>Nombres</th>
        <th>Licencia</th>
        <th>Direccion</th>
        <th>telefonoFijo</th>
        <th>Celular</th>
        
        <th>Modificar</th>
        <th>Eliminar</th>
      </tr>
    </thead>
    <tbody>

    <?php
    $indice=1;
    foreach ($conductor->result() as $row) {
      ?>
      <tr>
        <td><?php echo $indice; ?></td>
        <td><?php echo $row->primerApellido; ?></td>
        <td><?php echo $row->segundoApellido; ?></td>
        <td><?php echo $row->nombres; ?></td>
        <td><?php echo $row->numeroLicencia; ?></td>
        <td><?php echo $row->direccion; ?></td>
        <td><?php echo $row->telefonoFijo; ?></td>
        <td><?php echo $row->celular; ?></td>
        <td>

          <?php echo form_open_multipart('principal/conductormodificar'); ?>
           
           <input type="hidden" name="idConductor" value="<?php echo $row->idConductor; ?>"></input>
            
            <button type="submit" class="btn btn-primary">Modificar</button>
            
           <?php echo form_close(); ?>


        </td>
         <td>
          <?php echo form_open_multipart('principal/eliminarConductordb'); ?>
           

            <input type="hidden" name="idConductor" value="<?php echo $row->idConductor; ?>"></input>           
            <input type="hidden" name="primerApellido" value="<?php echo $row->primerApellido; ?>"></input>
                 <input type="hidden" name="nombres" value="<?php echo $row->nombres; ?>"></input>
            <button type="submit" class="btn btn-primary">eliminar</button>
            
            <?php echo form_close(); ?>
        </td>
      </tr>
      <?php
      $indice++;
    }
    ?>
    </tbody>
  </table> 

 </div>