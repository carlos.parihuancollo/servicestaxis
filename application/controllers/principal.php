<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Principal extends CI_Controller{

	

	public function index()
	{
	//$this->load->view('head');
		$this->load->view('vistaprincipal');
	//	$this->load->view('footer');
	}

	public function inicioGestion()
	{
		$this->load->view('head');
		$this->load->view('verPrincipal');
		$this->load->view('footer');
	}
	public function conductor()
	{
		$this->load->view('head');
		$this->load->view('vistaconductor');
		$this->load->view('footer');
	}
	public function taxi()
	{
		$this->load->view('head');
		$this->load->view('vistataxi');
		$this->load->view('footer');
	}
	public function empresa()
	{
		$this->load->view('head');
		$this->load->view('vistaempresa');
		$this->load->view('footer');
	}
	public function conductorgestion()
	{
		$this->load->view('head');
		$data['conductor']=$this->conductor_model->retornarConductor();
		$this->load->view('listaconductores',$data);
		$this->load->view('footer');
	}

	public function eliminarConductordb()
	{
		$idConductor=$_POST['idConductor'];
		$primerApellido=$_POST['primerApellido'];
		$data['primerApellido']=$primerApellido;
		

		$this->conductor_model->eliminarConductor($idConductor);
		$this->load->view('head');
		
		
		redirect('principal/conductorgestion');
		$this->load->view('footer');
		
	}	

	public function conductormodificar()
	{
		$idConductor=$_POST['idConductor'];
		$data['condu']=$this->conductor_model->recuperarConductor($idConductor);
		$this->load->view('head');
		$this->load->view('modificarconductorform',$data);
		$this->load->view('footer');
	}


	public function modificarconductordb()
	{
		$idConductor=$_POST['idConductor'];
		$primerApellido=$_POST['primerApellido'];
		$data['primerApellido']=$primerApellido;
		$segundoApellido=$_POST['segundoApellido'];
		$data['segundoApellido']=$segundoApellido;
		$nombres=$_POST['nombres'];
		$data['nombres']=$nombres;
		$numeroLicencia=$_POST['numeroLicencia'];
		$data['numeroLicencia']=$numeroLicencia;
		$direccion=$_POST['direccion'];
		$data['direccion']=$direccion;
		$telefonoFijo=$_POST['telefonoFijo'];
		$data['telefonoFijo']=$telefonoFijo;
		$celular=$_POST['celular'];
		$data['celular']=$celular;



		$this->conductor_model->modificarConductor($idConductor,$data);
		$this->load->view('head');
		//$this->load->view('modificarpaismensaje',$data);
	redirect('principal/conductorgestion');
		$this->load->view('footer');

	}


	public function agregarConductor()
	{

		$this->load->view('head');
		$this->load->view('agregarConductorform');
		$this->load->view('footer');
	}
	public function agregarConductordb()
	{
		
		$primerApellido=$_POST['primerApellido'];
		$data['primerApellido']=$primerApellido;
		$segundoApellido=$_POST['segundoApellido'];
		$data['segundoApellido']=$segundoApellido;
		$nombres=$_POST['nombres'];
		$data['nombres']=$nombres;
		$numeroLicencia=$_POST['numeroLicencia'];
		$data['numeroLicencia']=$numeroLicencia;
		$direccion=$_POST['direccion'];
		$data['direccion']=$direccion;
		$telefonoFijo=$_POST['telefonoFijo'];
		$data['telefonoFijo']=$telefonoFijo;
		$celular=$_POST['celular'];
		$data['celular']=$celular;


		
		$this->conductor_model->agregarConductor($data);
		$this->load->view('head');
		//$this->load->view('agregarmensaje',$data);
		redirect('principal/conductorgestion');
		$this->load->view('footer');

	}

		public function login()
		{
			//$this->load->view('head');
			$this->load->view('vistaLoginAdmin');
			//$this->load->view('footer');
		}


	public function taxigestion()
	{
		$this->load->view('head');
		$data['taxi']=$this->taxi_model->retornarTaxi();
		$this->load->view('listataxis',$data);
		$this->load->view('footer');
	}

	public function eliminarTaxidb()
	{
		$idtaxi=$_POST['idtaxi'];
		$numeroPlaca=$_POST['numeroPlaca'];
		$data['numeroPlaca']=$numeroPlaca;
		

		$this->taxi_model->eliminarTaxi($idtaxi);
		$this->load->view('head');
		redirect('principal/taxigestion');
		$this->load->view('footer');

	}	

	public function taximodificar()
	{
		$idtaxi=$_POST['idtaxi'];
		$data['condu']=$this->taxi_model->recuperarTaxi($idtaxi);
		$this->load->view('head');
		$this->load->view('modificartaxiform',$data);
		$this->load->view('footer');
	}


	public function modificartaxidb()
	{
		$idtaxi=$_POST['idtaxi'];
		$numeroPlaca=$_POST['numeroPlaca'];
		$data['numeroPlaca']=$numeroPlaca;
		$numeroMovil=$_POST['numeroMovil'];
		$data['numeroMovil']=$numeroMovil;
		$idtipoVehiculo=$_POST['idtipoVehiculo'];
		$data['idtipoVehiculo']=$idtipoVehiculo;
		

		$this->taxi_model->modificarTaxi($idtaxi,$data);
		$this->load->view('head');
		//$this->load->view('modificarpaismensaje',$data);
	redirect('principal/taxigestion');
		$this->load->view('footer');

	}


	public function agregarTaxi()
	{

		$this->load->view('head');
		$this->load->view('agregarTaxiform');
		$this->load->view('footer');
	}
	public function agregarTaxidb()
	{
		
		$numeroPlaca=$_POST['numeroPlaca'];
		$data['numeroPlaca']=$numeroPlaca;
		$numeroMovil=$_POST['numeroMovil'];
		$data['numeroMovil']=$numeroMovil;
		$idtipoVehiculo=$_POST['idtipoVehiculo'];
		$data['idtipoVehiculo']=$idtipoVehiculo;


		
		$this->taxi_model->agregarTaxi($data);
		$this->load->view('head');
		//$this->load->view('agregarmensaje',$data);
		redirect('principal/taxigestion');
		$this->load->view('footer');

	}
}
